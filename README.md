# N Queens on NxN chessboard

Solution is implemented in [queens.js](src/queens.js) file with some utils in [board-utils.js](src/board-utils.js) file.

## Algorithm
For every row a Queen is placed at one of the available positions, and then all
the attacked positions for the whole board are marked in a map. For the next row
a queen can choose again only from the available "non-conflict" positions.
If all the queens could have been placed, a solution was found and solution
count is increased.

### Possible improvements
Solution could be improved by limiting the available position recalculations
to only the next row, as for many possibilities the algorithm wouldn't touch
the last rows. Another improvement could be to take advantage of the symmetry
of the board.

## Combinatorial approach

An alternative solution was explored in the [combinatorial branch](https://bitbucket.org/marianboda/queens/src/f22235d5ecc4022fe43f6cf2170f1f0fe3f507ae/?at=combinatorial).
Source code of this solution is much shorter, but runs slower than the map-based solution.

One of the reasons may be the efficiency of the calculations of permutations.

## How to run
```
npm i

npm start n
```
where n is the size of the board and number of queens

## Demo
Live demo is located [here](http://vacuum.boda.one/queens).
