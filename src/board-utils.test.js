import {
  getMainDiagonal,
  getContraDiagonal,
  getDiagonalPoints
} from './board-utils'

test('getMainDiagonal gets points along main diagonal from the point', () => {
  expect(getMainDiagonal([3,4], 8)).toEqual([
    [0, 1],
    [1, 2],
    [2, 3],
    [3, 4],
    [4, 5],
    [5, 6],
    [6, 7],
  ])

  expect(getMainDiagonal([5,5], 8)).toEqual([
    [0, 0],
    [1, 1],
    [2, 2],
    [3, 3],
    [4, 4],
    [5, 5],
    [6, 6],
    [7, 7],
  ])

  expect(getMainDiagonal([5,1], 8)).toEqual([
    [4, 0],
    [5, 1],
    [6, 2],
    [7, 3],
  ])
})


test('getContraDiagonal gets points along contra-diagonal from the point', () => {
  expect(getContraDiagonal([3,4], 8)).toEqual([
    [0, 7],
    [1, 6],
    [2, 5],
    [3, 4],
    [4, 3],
    [5, 2],
    [6, 1],
    [7, 0],
  ])

  expect(getContraDiagonal([6,6], 8)).toEqual([
    [5, 7],
    [6, 6],
    [7, 5],
  ])

  expect(getContraDiagonal([2,3], 8)).toEqual([
    [0, 5],
    [1, 4],
    [2, 3],
    [3, 2],
    [4, 1],
    [5, 0],
  ])
})

test('getMainDiagonal gets points along main diagonal from the point', () => {
  expect(getDiagonalPoints([3,4], 8)).toEqual([
    [0, 1],
    [1, 2],
    [2, 3],
    [3, 4],
    [4, 5],
    [5, 6],
    [6, 7],
    [0, 7],
    [1, 6],
    [2, 5],
    [3, 4],
    [4, 3],
    [5, 2],
    [6, 1],
    [7, 0],
  ])
  expect(getDiagonalPoints([1,0], 8)).toEqual([
    [1, 0],
    [2, 1],
    [3, 2],
    [4, 3],
    [5, 4],
    [6, 5],
    [7, 6],
    [0, 1],
    [1, 0],
  ])
})
