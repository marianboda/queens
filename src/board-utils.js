import R from 'ramda'

export const getMainDiagonal = (point, n) => {
  const [row, col] = point
  const min = Math.min(row, col)

  let i = row - min
  let j = col - min

  let result = []
  while (i < n && j < n) {
    result.push([i, j])
    i += 1
    j += 1
  }
  return result
}

export const getContraDiagonal = (point, n) => {
  const f = ([i, j]) => [
    Math.max(0, i + j - (n - 1)),
    Math.min(i + j, n - 1),
  ]

  let [i, j] = f(point)

  let result = []
  while (i < n && j >= 0) {
    result.push([i, j])
    i += 1
    j -= 1
  }
  return result
}

export const getDiagonalPoints = (point, matrixSize) => {
  return R.concat(
    getMainDiagonal(point, matrixSize),
    getContraDiagonal(point, matrixSize)
  )
}

export const printBoard = (board) => {
  board.forEach(i => {
    console.log(i.map(v => v ? '.' : 0).join(''))
  })
}

export const clearPoint = (board, [i, j]) => {
  board[i][j] = false
}

export const clearRow = (board, i) => {
  for (let j = 0; j < board.length; j++) {
    clearPoint(board, [i, j])
  }
}

export const clearCol = (board, j) => {
  for (let i = 0; i < board.length; i++) {
    clearPoint(board, [i, j])
  }
}

export const clearDiagonals = (board, q) => {
  getDiagonalPoints(q, board.length)
    .forEach(R.partial(clearPoint, [board]))
}

export const placeQueen = (board, q) => {
  const [ row, col ] = q
  const newBoard = R.clone(board)
  clearRow(newBoard, row)
  clearCol(newBoard, col)
  clearDiagonals(newBoard, q)
  return newBoard
}

export const createBoard = (size) => {
  let board = []
  for (let i = 0; i < size; i++) {
    board.push(R.repeat(true, size))
  }
  return board
}
