import { append } from 'ramda'
import {
  createBoard,
  placeQueen,
} from './board-utils'

// Returns available positions for given row. I used reduce instead of filter,
// because filter would need to use map before and also after.
// (Data for a row is just an array of booleans)
const getAvailablePoints = (board, row) => {
  return board[row].reduce((acc, val, i) => {
    return val ? append(i, acc) : acc
  }, [])
}

// Recursive function to process the whole board row by row.
const processRow = (board, row) => {
  const availablePoints = getAvailablePoints(board, row)
  const n = board.length

  // If we got to the last row, and there is an available position to
  // place a queen, we have found a solution.
  let solutionCount = (row === n - 1 && availablePoints.length === 1) ? 1 : 0

  // If we are on the last row, there is nothing more to explore.
  if (row === n - 1) {
    return solutionCount
  }

  // For every available point we'll explore the possibility to
  // place a Queen there and processing the next row
  availablePoints.forEach((p) => {
    // to place a Queen means to update the board flagging the attack zone.
    // placeQueen() function returns an updated board without modifying the original
    const innerBoard = placeQueen(board, [row, p])
    solutionCount += processRow(innerBoard, row + 1)
  })
  return solutionCount
}

export const calculateSolutions = (n) => {
  const board = createBoard(n)
  return processRow(board, 0)
}
