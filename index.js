import { calculateSolutions } from './src/queens'

// We get n from commandline, or use 8 if not provided
const args = process.argv.slice(2)
const n = (args.length > 0) ? Number(args[0]) : 8

const solutionCount = calculateSolutions(n)

console.log(`${n} queens on ${n}x${n} board: ${solutionCount} solutions`)
